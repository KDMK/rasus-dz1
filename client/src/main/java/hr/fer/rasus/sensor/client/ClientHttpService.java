package hr.fer.rasus.sensor.client;

import hr.fer.rasus.sensor.model.MeasurementDTO;
import hr.fer.rasus.sensor.model.Reading;
import hr.fer.rasus.sensor.model.Sensor;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.apache.logging.log4j.LogManager.getLogger;

class ClientHttpService {

    private static final Logger log = getLogger(ClientHttpService.class);

    private static final String SERVER_ADDRESS = "http://localhost:8080";
    private static final String SERVER_OPERATION_NEIGHBOUR = "api/sensor/operation/neighbour/";
    private static final String OPERATION_REGISTER = "api/sensor/operation/register";
    private static final String OPERATION_SEND_MEASUREMENT = "api/sensor/operation/store-measurement";

    private Client client;

    ClientHttpService(Client client) {
        this.client = client;
    }

    void registerSensor(Sensor sensor) {
        Invocation.Builder request = createJerseryInvocation(OPERATION_REGISTER);

        log.info("Posting registration request to server {}", SERVER_ADDRESS);
        Response response;
        try {
            response = request.post(Entity.entity(sensor, MediaType.APPLICATION_JSON));
        } catch (Exception e) {
            log.error("Connection refuse while contacting server. Retrying registration at next reading.");
            return;
        }

        if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
            log.info("Succesfully registered sensor at server {}", SERVER_ADDRESS);
            client.setRegistered(true);
        } else {
            log.error("Error registering at server {}. Exiting...", SERVER_ADDRESS);
            System.exit(1);
        }
    }

    Sensor getNeighbour(Sensor sensor) {
        Invocation.Builder request = createJerseryInvocation(SERVER_OPERATION_NEIGHBOUR + sensor.getUsername());

        Response response;
        try {
            response = request.get();
        } catch (Exception e) {
            log.error("Connection refuse while contacting server. Cannot get neighbour data");
            return null;
        }

        if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
            Sensor neighbour = response.readEntity(Sensor.class);
            log.info("Succesfully got neighbour {}", neighbour);
            return neighbour;
        }

        log.error("Cannot get neighbour!");
        return null;
    }

    void sendMeasurement(Reading readings, Sensor sensor) {
        Invocation.Builder request = createJerseryInvocation(OPERATION_SEND_MEASUREMENT);

        MeasurementDTO dto = new MeasurementDTO();
        dto.setReadings(readings);
        dto.setUsername(sensor.getUsername());

        log.info("Sending measurements to server {}", SERVER_ADDRESS);
        try {
            request.post(Entity.entity(dto, MediaType.APPLICATION_JSON));
        } catch (Exception e) {
            log.error("Connection refuse while contacting server. Skipping sending data.");
            client.setRegistered(false);
        }
    }

    private Invocation.Builder createJerseryInvocation(String operationRegister) {
        javax.ws.rs.client.Client client = ClientBuilder.newClient();

        WebTarget resource = client.target(SERVER_ADDRESS);
        resource = resource.path(operationRegister);

        Invocation.Builder request = resource.request();
        request.accept(MediaType.APPLICATION_JSON);
        return request;
    }
}

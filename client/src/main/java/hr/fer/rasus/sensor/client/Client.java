package hr.fer.rasus.sensor.client;

import hr.fer.rasus.sensor.model.Reading;
import hr.fer.rasus.sensor.model.Sensor;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

import static hr.fer.rasus.sensor.client.ClientUtil.calcAvg;
import static hr.fer.rasus.sensor.client.ClientUtil.getReadingsFromFile;
import static java.lang.Thread.sleep;
import static org.apache.logging.log4j.LogManager.getLogger;

public class Client implements Runnable {

    private static final Logger log = getLogger(Client.class);

    private static List<Reading> readings = getReadingsFromFile();

    @Getter @Setter private boolean registered;
    private Socket neighbour;
    private ObjectOutputStream neighbourInputStream;
    private ObjectInputStream neighbourOutputStream;
    private long lastNeighbourCheckTime;
    private final Sensor sensor;
    private ClientHttpService httpService;

    public Client(Sensor sensor) {
        this.sensor = sensor;
        this.httpService = new ClientHttpService(this);
    }

    @Override
    public void run() {
        long startTime = System.nanoTime();

        while (true) {
            if (!registered) httpService.registerSensor(sensor);

            long now = System.nanoTime();
            processNeighbour(now);
            Reading avgReading = getNextReading(startTime, now);
            httpService.sendMeasurement(avgReading, sensor);

            try {
                sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Reading getNextReading(long startTime, long now) {
        long elapsedTime = now - startTime;
        Reading currentReading = readings.get((int) (elapsedTime % 100));
        sensor.setCurrentReading(currentReading);

        Reading fromNeighbour = neighbour != null ? getReadingFromNeighbour() : null;
        Reading avgReading = calculateAvgReading(fromNeighbour, currentReading);

        log.info("My measurement: {}", currentReading);
        log.info("My neighbour measurement: {}", fromNeighbour);
        log.info("Avg measurement: {}", avgReading);

        return avgReading;
    }

    private void processNeighbour(long now) {
        long t = now - lastNeighbourCheckTime;
        if (t / 10e5 > 5000) {
            handleNeighbourChange(sensor);
            lastNeighbourCheckTime = now;
        }
    }

    private Reading calculateAvgReading(Reading fromNeighbour, Reading currentReading) {
        if (fromNeighbour == null) {
            return currentReading;
        }

        return Reading.builder().temperature(calcAvg(currentReading.getTemperature(), fromNeighbour.getTemperature()))
                      .humidity(calcAvg(currentReading.getHumidity(), fromNeighbour.getHumidity()))
                      .pressure(calcAvg(currentReading.getPressure(), fromNeighbour.getPressure()))
                      .co(calcAvg(currentReading.getCo(), fromNeighbour.getCo()))
                      .no2(calcAvg(currentReading.getNo2(), fromNeighbour.getNo2()))
                      .so2(calcAvg(currentReading.getSo2(), fromNeighbour.getSo2())).build();
    }

    private Reading getReadingFromNeighbour() {
        Reading neighbourReading = null;

        try {
            neighbourInputStream.writeObject("sync");
            neighbourReading = (Reading) neighbourOutputStream.readObject();
        } catch (IOException e) {
            log.info("Lost connection to neighbour {}", sensor.getNearestNeighbour().getUsername());
            sensor.setNearestNeighbour(null);
            lastNeighbourCheckTime = 0;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return neighbourReading;
    }

    private void handleNeighbourChange(Sensor sensor) {
        Sensor oldNeighbour = sensor.getNearestNeighbour();
        Sensor newNeighbour = httpService.getNeighbour(sensor);

        if (newNeighbour != null) {
            if (oldNeighbour == null || !oldNeighbour.getUsername().equals(newNeighbour.getUsername())) {
                sensor.setNearestNeighbour(newNeighbour);
                log.info("Got new neighbour {}", newNeighbour.getUsername());

                if (neighbour != null && !neighbour.isClosed()) {
                    try {
                        neighbourOutputStream.close();
                        neighbourInputStream.close();
                        neighbour.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    neighbour = new Socket(newNeighbour.getIPAddress(), newNeighbour.getPort());
                    neighbourOutputStream = new ObjectInputStream(neighbour.getInputStream());
                    neighbourInputStream = new ObjectOutputStream(neighbour.getOutputStream());
                } catch (IOException e) {
                    log.error("Cannot connect to neighbour {}", newNeighbour.getUsername());
                    neighbour = null;
                    sensor.setNearestNeighbour(null);
                }
            }
        }
    }


}
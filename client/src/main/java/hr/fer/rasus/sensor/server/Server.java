package hr.fer.rasus.sensor.server;

import hr.fer.rasus.sensor.model.Sensor;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.apache.logging.log4j.LogManager.getLogger;

public class Server implements IServer {

    private static final Logger log = getLogger(Server.class);

    public static int PORT = 15643;
    private static final int NUMBER_OF_THREADS = 4;
    private static final int BACKLOG = 10;

    private final AtomicInteger activeConnections;
    private ServerSocket serverSocket;
    private final ExecutorService executor;
    private final AtomicBoolean runningFlag;
    private final Sensor sensor;

    public Server(Sensor sensor) {
        activeConnections = new AtomicInteger(0);
        executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
        runningFlag = new AtomicBoolean(false);

        this.sensor = sensor;
    }

    @Override
    public void startup() {
        try {
            this.serverSocket = new ServerSocket(PORT, BACKLOG);

            serverSocket.setSoTimeout(500);
            runningFlag.set(true);
            log.info("Server is ready!");

        } catch (SocketException e1) {
            log.error("Exception caught when setting server, socket timeout: {}", e1);
        } catch (IOException e2) {
            log.error("Exception caught when opening or setting the server socket: {}", e2);
        }
    }

    @Override
    public void loop() {

        while (runningFlag.get()) {
            try {
                // listen for a connection to be made to server socket from a sensor
                // accept connection and create a new active socket which communicates with the sensor
                Socket clientSocket = serverSocket.accept();

                // execute a new request handler in a new thread
                Runnable worker = new Worker(clientSocket, runningFlag, activeConnections, sensor);
                executor.execute(worker);
                //increment the number of active connections
                activeConnections.getAndIncrement();
            } catch (SocketTimeoutException ste) {
                // do nothing, check the runningFlag flag
            } catch (IOException e) {
                log.error("Exception caught when waiting for a connection: " + e);
            }
        }
    }

    @Override
    public void shutdown() {
        while (activeConnections.get() > 0) {
            log.warn("WARNING: There are still active connections"); //Need to wait!
            try {
                Thread.sleep(5000);
            } catch (java.lang.InterruptedException ignored) {
            }
        }
        if (activeConnections.get() == 0) {
            log.info("Starting server shutdown.");
            try {
                serverSocket.close(); /*CLOSE the main server socket*/
            } catch (IOException e) {
                log.error("Exception caught when closing the server socket: {}", e);
            } finally {
                executor.shutdown();
            }

            log.info("Server has been shutdown.");
        }
    }

    @Override
    public boolean getRunningFlag() {
        return runningFlag.get();
    }

}
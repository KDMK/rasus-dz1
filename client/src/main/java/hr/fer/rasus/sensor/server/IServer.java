package hr.fer.rasus.sensor.server;

public interface IServer {
    // Server startup. Starts all services offered by the server.
    void startup();

    // Server loops when in running mode. The server must be active
    // to accept sensor requests.
    void loop();

    // Server shutdown. Shuts down all services started during
    // startup.
    void shutdown();

    // Gets the running flag that indicates server running status.
    // @return running flag
    boolean getRunningFlag();

}

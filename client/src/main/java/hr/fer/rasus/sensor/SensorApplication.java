package hr.fer.rasus.sensor;

import hr.fer.rasus.sensor.client.Client;
import hr.fer.rasus.sensor.model.Sensor;
import hr.fer.rasus.sensor.server.IServer;
import hr.fer.rasus.sensor.server.Server;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;

import static org.apache.logging.log4j.LogManager.getLogger;

public class SensorApplication {

    private static final Logger log = getLogger(SensorApplication.class);

    public static void main(String args[]) {
        selectFreePort();

        Sensor sensor = new Sensor(true);
        log.info("Started new sensor application on port {}", sensor.getPort());
        log.info("Created sensor with id {} on location - lat: {}; lon: {}", sensor.getUsername(), sensor.getLatitude(),
                 sensor.getLongitude());

        Thread sensorClient = new Thread(new Client(sensor));
        IServer sensorServer = new Server(sensor);

        sensorServer.startup();
        sensorClient.start();
        sensorServer.loop();
        sensorServer.shutdown();
    }

    private static int selectFreePort() {
        int port = Server.PORT;

        boolean foundPort = false;
        while (!foundPort) {
            try (ServerSocket ignored = new ServerSocket(port)) {
            } catch (IOException e) {
                ++port;
             } finally {
                foundPort = true;
            }
        }

        Server.PORT = port;
        return port;
    }
}

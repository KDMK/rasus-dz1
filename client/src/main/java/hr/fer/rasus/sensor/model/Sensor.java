package hr.fer.rasus.sensor.model;

import hr.fer.rasus.sensor.server.Server;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.UUID;

import static hr.fer.rasus.sensor.client.ClientUtil.setLatAndLong;
import static hr.fer.rasus.sensor.server.Server.PORT;
import static java.net.InetAddress.getLocalHost;
import static org.apache.logging.log4j.LogManager.getLogger;

@Data
@NoArgsConstructor
public class Sensor {

    private static final Logger log = getLogger(Sensor.class);

    private String username;
    private Double longitude;
    private Double latitude;
    private String IPAddress;
    private Reading currentReading;
    private int port;

    private Map<String, Float> measurements;
    private Sensor nearestNeighbour;

    public Sensor(boolean autoConfigure) {
        if(!autoConfigure) return;

        setLatAndLong(this);
        this.port = Server.PORT;
        this.username = String.valueOf(UUID.randomUUID());
        try {
            this.IPAddress = getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.error("Cannot get host address!");
            e.printStackTrace();
            System.exit(1);
        }
    }
}

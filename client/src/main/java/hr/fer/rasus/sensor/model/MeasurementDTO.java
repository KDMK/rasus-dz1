package hr.fer.rasus.sensor.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MeasurementDTO {
    private String username;
    private Reading readings;
}
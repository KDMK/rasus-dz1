package hr.fer.rasus.sensor.server;

import hr.fer.rasus.sensor.model.Reading;
import hr.fer.rasus.sensor.model.Sensor;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.apache.logging.log4j.LogManager.getLogger;

public class Worker implements Runnable {

    private static final Logger log = getLogger(Worker.class);

    private final Socket clientSocket;
    private final AtomicBoolean isRunning;
    private final AtomicInteger activeConnections;
    private final Sensor sensor;

    Worker(Socket clientSocket, AtomicBoolean isRunning, AtomicInteger activeConnections, Sensor sensor) {
        this.clientSocket = clientSocket;
        this.isRunning = isRunning;
        this.activeConnections = activeConnections;
        this.sensor = sensor;
    }

    @Override
    public void run() {
        log.info("Started new worker");
        try (ObjectOutputStream outToClient = new ObjectOutputStream(clientSocket.getOutputStream());
             ObjectInputStream inFromClient = new ObjectInputStream(clientSocket.getInputStream())) {
            log.info("Created input and output streams");
            Object receivedObject;

            while (true) {
                receivedObject = inFromClient.readObject();
                log.info("Server received: " + receivedObject);

                //shutdown the server if requested
                if (receivedObject.getClass().equals(String.class) && ((String) receivedObject).contains("shutdown")) {
                    outToClient.writeChars("Initiating server shutdown!");//WRITE
                    log.info("Server sent: Initiating server shutdown!");
                    isRunning.set(false);
                    activeConnections.getAndDecrement();
                    return;
                }

                if (receivedObject.getClass().equals(String.class) && ((String) receivedObject).contains("sync")) {
                    Reading reading = sensor.getCurrentReading();
                    log.info("Sending {} to {}", reading, clientSocket.getInetAddress(), clientSocket.getPort());
                    outToClient.writeObject(reading); //WRITE
                }
            }
        } catch (IOException ex) {
            log.error("Exception caught when trying to read or send data: {}", ex.getMessage());
        } catch (ClassNotFoundException e) {
            log.error("Cannot find class {}", e.getCause());
        }
    }

}

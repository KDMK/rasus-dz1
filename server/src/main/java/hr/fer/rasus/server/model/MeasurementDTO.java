package hr.fer.rasus.server.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class MeasurementDTO {
    private String username;
    private Map<String, Float> readings;
}
package hr.fer.rasus.server.job;

import hr.fer.rasus.server.dao.SensorRepository;
import hr.fer.rasus.server.model.Sensor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class SensorJob {

    private SensorRepository sensorRepository;

    @Autowired
    public SensorJob(SensorRepository sensorRepository) {
        this.sensorRepository = sensorRepository;
    }

    @Scheduled(initialDelay = 5000, fixedRate = 10000)
    public synchronized void cleanInactiveSensor() {
        log.info("Clean inactive sensors job started.");
        List<Sensor> sensors = sensorRepository.getAllSensors();

        long now = System.nanoTime();

        for (Sensor s : sensors) {
            if (s.getLastMeasurementTime() == null) {
                sensorRepository.removeSensor(s.getUsername());
            }

            long t = now - s.getLastMeasurementTime();
            if (t / 10e5 > 10000) {
                sensorRepository.removeSensor(s.getUsername());
                log.info("Removed inactive sensor {}", s);
            }
        }

        log.info("Clean inactive sensors job finished.");
    }
}

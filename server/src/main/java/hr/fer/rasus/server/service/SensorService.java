package hr.fer.rasus.server.service;

import hr.fer.rasus.server.model.Sensor;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface SensorService {

    Sensor findNearestNeighbour(String username);
    void storeMeasurement(String username, String parameter, Float averageValue);
    void storeMeasurement(String username, Map<String, Float> readings);
    void registerSensor(String username, double latitude, double longitude, String IPaddress, int port);

    List<Sensor> getAllSensors();
    Optional<Sensor> getSensor(String username);
}

package hr.fer.rasus.server.dao;

import hr.fer.rasus.server.model.Sensor;

import java.util.List;
import java.util.Optional;

public interface SensorRepository {

    List<Sensor> getAllSensors();
    Optional<Sensor> getSensor(String username);
    void saveSensor(Sensor newSensor);
    void removeSensor(String username);
}

package hr.fer.rasus.server.service;

import hr.fer.rasus.server.dao.SensorRepository;
import hr.fer.rasus.server.model.Sensor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.lang.Math.*;

@Service
public class SensorServiceImpl implements SensorService {

    private SensorRepository sensorRepository;

    @Autowired
    public SensorServiceImpl(SensorRepository sensorRepository) {
        this.sensorRepository = sensorRepository;
    }

    @Override
    public Sensor findNearestNeighbour(String username) {
        List<Sensor> allSensors = sensorRepository.getAllSensors();
        Optional<Sensor> mySensor = sensorRepository.getSensor(username);

        final Sensor[] nearestNeighbor = {null};
        final double[] minimalDistance = {Double.MAX_VALUE};

        mySensor.ifPresent(sensor1 -> allSensors.forEach(sensor -> {
            if (!sensor.getUsername().equals(username)) {
                double distance = calculateDistance(sensor, sensor1);
                if (distance < minimalDistance[0]) {
                    minimalDistance[0] = distance;
                    nearestNeighbor[0] = sensor;
                }
            }
        }));

        return nearestNeighbor[0];
    }

    private double calculateDistance(Sensor sensor, Sensor mySensor) {
        int R = 6371;

        double dlon = sensor.getLongitude() - mySensor.getLongitude();
        double dlat = sensor.getLatitude() - mySensor.getLatitude();
        double a = (pow(sin(dlat / 2), 2) + cos(sensor.getLatitude()) * cos(mySensor.getLatitude()) * (pow(sin(dlon / 2), 2)));
        double c = 2 * atan2(sqrt(a), sqrt(1 - a));

        return R * c;
    }

    @Override
    public void storeMeasurement(String username, String parameter, Float averageValue) {
        sensorRepository.getSensor(username).ifPresent(sensor -> {
            sensor.getMeasurements().put(parameter, averageValue == null ? -1.0F : averageValue);
            sensor.setLastMeasurementTime(System.nanoTime());
        });
    }

    @Override
    public void storeMeasurement(String username, Map<String, Float> readings) {
        readings.forEach((parameter, value) -> storeMeasurement(username, parameter, value));
    }

    @Override
    public void registerSensor(String username, double latitude, double longitude, String IPaddress, int port) {
        Sensor newSensor = new Sensor();
        newSensor.setUsername(username);
        newSensor.setLatitude(latitude);
        newSensor.setLongitude(longitude);
        newSensor.setIPAddress(IPaddress);
        newSensor.setPort(port);

        sensorRepository.saveSensor(newSensor);
    }

    @Override
    public List<Sensor> getAllSensors() {
        return sensorRepository.getAllSensors();
    }

    @Override
    public Optional<Sensor> getSensor(String username) {
        return sensorRepository.getSensor(username);
    }
}

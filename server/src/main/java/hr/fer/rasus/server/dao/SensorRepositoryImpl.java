package hr.fer.rasus.server.dao;

import hr.fer.rasus.server.model.Sensor;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class SensorRepositoryImpl implements SensorRepository {

    private Map<String, Sensor> sensors = new HashMap<>();

    @Override
    public List<Sensor> getAllSensors() {
        return new ArrayList<>(sensors.values());
    }

    @Override
    public Optional<Sensor> getSensor(String username) {
        Sensor sensor = sensors.get(username);
        return Optional.ofNullable(sensor);
    }

    @Override
    public void saveSensor(Sensor newSensor) {
        if(sensors.containsKey(newSensor.getUsername())) {
            throw new IllegalArgumentException("Sensor with given username already exists");
        }

        this.sensors.put(newSensor.getUsername(), newSensor);
    }

    @Override
    public void removeSensor(String username) {
        sensors.remove(username);
    }
}

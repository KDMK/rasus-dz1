package hr.fer.rasus.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
public class Sensor implements Serializable {

    private String username;
    private double longitude;
    private double latitude;
    private String IPAddress;
    private int port;
    private Map<String, Float> measurements = new HashMap<>();
    @JsonIgnore() private Long lastMeasurementTime;
}

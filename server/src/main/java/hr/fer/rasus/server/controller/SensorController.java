package hr.fer.rasus.server.controller;

import hr.fer.rasus.server.model.MeasurementDTO;
import hr.fer.rasus.server.model.Sensor;
import hr.fer.rasus.server.service.SensorService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping("/api/sensor")
public class SensorController {

    private static final Logger log = getLogger(SensorController.class);

    private SensorService sensorService;

    @Autowired
    public SensorController(SensorService sensorService) {
        this.sensorService = sensorService;
    }


    @GetMapping
    public ResponseEntity<List<Sensor>> getAllSensors(HttpServletRequest req) {
        log.info("Got request for all sensors from {}", req.getRemoteAddr());
        return ResponseEntity.ok(sensorService.getAllSensors());
    }

    @GetMapping("/{username}")
    public ResponseEntity<Sensor> getSensors(@PathVariable String username, HttpServletRequest req) {
        log.info("Got request sensor {} from {}", username, req.getRemoteAddr());
        return ResponseEntity.ok(sensorService.getSensor(username).orElse(null));
    }

    @PostMapping("/operation/register")
    public ResponseEntity<String> register(@RequestBody Sensor sensor, HttpServletRequest req) {
        log.info("Got registration request {} from {}", sensor, req.getRemoteAddr());

        try {
            sensorService.registerSensor(sensor.getUsername(), sensor.getLatitude(),
                    sensor.getLongitude(), sensor.getIPAddress(), sensor.getPort());
        } catch (IllegalArgumentException e) {
            log.error("Username {} already exists", sensor.getUsername());
            return ResponseEntity.badRequest().body(String.format("Username %s already exists!", sensor.getUsername()));
        }

        return ResponseEntity.ok("Succesfully registered!");
    }

    @GetMapping("/operation/neighbour/{username}")
    public ResponseEntity<Sensor> searchNeighbour(@PathVariable String username, HttpServletRequest req) {
        log.info("Got search neighbour request from {} at {}", username, req.getRemoteAddr());
        return ResponseEntity.ok(sensorService.findNearestNeighbour(username));
    }

    @PostMapping("/operation/store-measurement")
    public ResponseEntity<String> storeMeasurement(@RequestBody MeasurementDTO measurementDTO) {
        log.info("Got store measurement request {} from sensor {}", measurementDTO, measurementDTO.getUsername());

        try {
            sensorService.storeMeasurement(measurementDTO.getUsername(), measurementDTO.getReadings());
        } catch (IllegalArgumentException e) {
            log.error("Username {} doesn't exists!", measurementDTO.getUsername());
            return ResponseEntity.badRequest().body(String.format("Username %s doesn't exist!", measurementDTO.getUsername()));
        }

        return ResponseEntity.ok("Measurement stored");
    }
}
